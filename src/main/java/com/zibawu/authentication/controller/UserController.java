package com.zibawu.authentication.controller;

import com.zibawu.authentication.model.User;
import com.zibawu.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public ModelAndView registration() {
        ModelAndView model = new ModelAndView();
        User user = new User();
        model.addObject("user", user);
        model.setViewName("registration");

        return model;
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.POST)
    public ModelAndView createUser(@Valid User user, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        User userExists = userService.findByEmail(user.getEmail());

        if (userExists != null){
            bindingResult.rejectValue("username", "error", "This user already exists!");
        }
        if (bindingResult.hasErrors()) {
            model.setViewName("/registration");
        } else {
            userService.saveUser(user);
            model.addObject("msg","User has been registered successfully!");
            model.addObject("user", new User());
            model.setViewName("/registration");
        }
        return model;
    }

//    @RequestMapping(value = {"/menu"}, method = RequestMethod.GET)
//    public ModelAndView home() {
//        ModelAndView model = new ModelAndView();
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        User user = userService.findByEmail(auth.getName());
//
//        model.addObject("userName", user.getFirstName() + " " + user.getLastName());
////        model.addObject("userName", user.getEmail());
//        model.setViewName("/menu");
//        return model;
//    }

    @RequestMapping(value = {"/menu"},  method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView model = new ModelAndView();
        model.setViewName("/menu");
        return model;
    }

    @RequestMapping(value = {"/access_denied"}, method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView();
        model.setViewName("/access_denied");
        return model;
    }

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login(String error, String logout) {
        ModelAndView model = new ModelAndView();
        model.setViewName("login");
        if (error != null)
            model.addObject("error","Your username and/or password is invalid.");
        if (logout != null)
            model.addObject("message", "You have been logged out successfully.");
        return model;
    }

//    @RequestMapping("/")
//    public String login(){
//        return "login";
//    }
}